let Greeter = React.createClass({
    getDefaultProps: function () {
        return {
            name: 'React',
            message: 'It is my first react app!'
        }
    },
    getInitialState: function () {
        return {
            name: this.props.name
        };
    },
    onButtonClick: function (e) {
        e.preventDefault();
        let nameRef = this.refs.name;
        let name = nameRef.value;
        nameRef.value = '';
        if (typeof name === 'string' && name.length > 0) {
            this.setState({name});
        }
    },
    render: function () {
        let name = this.state.name,
            message = this.props.message;
        return (
            <div>
                <h1>Hello {name}</h1>
                <p>{message}</p>

                <form onSubmit={this.onButtonClick}>
                    <input type="text" ref="name"/>
                    <button>Set name</button>
                </form>
            </div>
        )
    }
});

ReactDOM.render(
    <Greeter name="Valentine"/>,
    document.getElementById('app')
);